def is_prime(n):
    """
    Check whether a number is prime or not
    """
    for i in range(2, n//2+1):
        if(n % i == 0):
            
            return False
    else:
        
        return True   

def n_digit_primes(digit = 2):
    """
    Return a list of `n_digit` primes using the `is_prime` function.

    Set the default value of the `digit` argument to 2
    """
    mylist = []
    for x in range(10*(digit-1), 10**digit):
        if is_prime(x) and x>1:
            mylist.append(x)
    return mylist    