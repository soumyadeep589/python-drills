
def word_count(s):
    """
    Find the number of occurrences of each word
    in a string(Don't consider punctuation characters)
    """
    pass
    
    worddict = {}
    mylist = s.split()
    
    for x in mylist:
        # print(x)
        if(x[-1] == '.' or x[-1] == ','):
            x = x[:-1]
        # print(x)
        if x in worddict:
            worddict[x] += 1
        else:
            worddict[x] = 1
    
#     print(worddict)
    return worddict
# s = "Python is an interpreted, high-level, general-purpose programming language. Python interpreters are available for many operating systems. Python is a multi-paradigm programming language. Object-oriented programming and structured programming are fully supported."
# word_count(s)
# word_count(s)
def dict_items(d):
    """
    Return a list of all the items - (key, value) pair tuples - of the dictionary, `d`
    """
    pass
    mylist = []
    for x in d:
        mylist.append((x, d[x]))
    return mylist

def dict_items_sorted(d):
    """
    Return a list of all the items - (key, value) pair tuples - of the dictionary, `d`
    sorted by `d`'s keys
    """
    pass
    mylist = []
    for x in sorted(d):
        mylist.append((x, d[x]))
    return mylist