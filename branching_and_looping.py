def integers_from_start_to_end_using_range(start, end, step):
    """return a list"""
    pass
    mylist = []
    for x in range(start, end, step):
        mylist.append(x)
    return mylist

def integers_from_start_to_end_using_while(start, end, step):
    """return a list"""
    
    mylist = []
    i = start
    if step > 0:
        while i < end:
            mylist.append(i)
            i = i + step
        return mylist
    else:
        while i > end:
            mylist.append(i)
            i += step
        return mylist

def two_digit_primes():
    """
    Return a list of all two-digit-primes
    """
    pass
    mylist = []
    for x in range(10,100):
        for i in range(2, x//2):
            if(x % i==0):
                break
        else:
            mylist.append(x)
    else:
        return mylist      
        