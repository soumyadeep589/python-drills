def unique(items):
    """
    Return a set of unique values belonging to the `items` list
    """
    return set(items)


def shuffle(items):
    """
    Shuffle all items in a list
    """
    import random

    itemsshuffled = random.shuffle(items)
    return itemsshuffled
def getcwd():
    """
    Get current working directory
    """
    import os
    cwd = os.getcwd()
    return cwd

def mkdir(name):
    """
    Create a directory at the current working directory
    """
    import os
    path = os.getcwd() + name

    os.mkdir(path)    