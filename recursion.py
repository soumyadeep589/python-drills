def html_dict_search(html_dict, selector):
    """
    Implement `id` and `class` selectors
    """
    mylist = []
    def dict_search(html_dict,selector):
        selector_new = selector[1:]
        print(selector_new)
        
        for x in html_dict['children']:
            print(x)
            print(x['attrs'])
            if len(x['attrs'].keys()) != 0:
                if 'class' in x["attrs"]: 
                    if x["attrs"]["class"] == selector_new:
                        mylist.append(x)
                        print(mylist)
                    else:
                        dict_search(x, selector)
                elif 'id' in x["attrs"]: 
                    if x["attrs"]["id"] == selector_new:
                        mylist.append(x)
                    else:
                        dict_search(x, selector)
            else:
                dict_search(x, selector)
    dict_search(html_dict, selector)
    return mylist


html_dict = {
  "name": "html",
  "attrs": {

  },
  "children": [
    {
      "name": "head",
      "attrs": {},
      "children": []
    },
    {
      "name": "body",
      "attrs": {},
      "children": [
        {
          "name": "div",
          "attrs": {
            "class": "container"
          },
          "children": [
            {
              "name": "ul",
              "attrs": {
                "id": "headlines"
              },
              "children": [
                {
                  "name": "li",
                  "attrs": {
                    "class": "headline-item"
                  },
                  "text": "zero",
                  "children": []
                },
                {
                  "name": "li",
                  "attrs": {
                    "class": "headline-item"
                  },
                  "text": "one",
                  "children": []
                },
                {
                  "name": "li",
                  "attrs": {
                    "class": "headline-item"
                  },
                  "text": "two",
                  "children": []
                },
                {
                  "name": "li",
                  "attrs": {
                    "class": "headline-item"
                  },
                  "text": "three",
                  "children": []
                }
              ]
            },
            {
              "name": "div",
              "attrs": {

              },
              "children": [
                {
                  "name": "div",
                  "attrs": {

                  },
                  "children": [
                    {
                      "name": "button",
                      "attrs": {
                        "id": "buy-now-btn"
                      },
                      "text": "Buy Now",
                      "children": []
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    }
  ]
}


output = html_dict_search(html_dict, '.headline-item')
print(output)