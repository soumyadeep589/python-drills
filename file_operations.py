"""
Implement the below file operations.
Close the file after each operation.
"""


def read_file(path):
    
    f = open(path)
    # print(type(f.read()))
    strng = f.read()
    f.close()
    return strng

# read_file("data.txt")

def write_to_file(path, s):
    
    f = open(path, "w")
    f.write(s)
    f.close()
    return f

def append_to_file(path, s):
    
    f = open(path, "a")
    f.write(s)
    f.close()
    return f

def numbers_and_squares(n, file_path):
    """
    Save the first `n` natural numbers and their squares into a file in the csv format.

    Example file content for `n=3`:

    1,1
    2,4
    3,9
    """
    import csv
    
    mylist = []
    for i in range(1,n+1):
        mylist.append([i, i**2])
    # csvData = [['Person', 'Age'], ['Peter', '22'], ['Jasmine', '21'], ['Sam', '24']]
    with open(file_path, 'w') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerows(mylist)
    csvFile.close()
